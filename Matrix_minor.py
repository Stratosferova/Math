
# coding: utf-8

# In[1]:

matrix = [[1.6, 2.5, 3.1], [7.7, 6.3, 4.7], [6.1, 4.0, 25.1]]

def det(matrix):
    n = len(matrix)
    if n == 1:
        return matrix[0][0]
    if n == 2:
        return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0]
    res = 0
    # i == 0
    for j in range(n):
        minor = []
        for i in range(1, n):
            minor.append([matrix[i][k] for k in range(n) if k != j])
        res += ((-1) ** j) * matrix[0][j] * det(minor)
    return res

            
def print_matrix(matrix):
    for i in matrix:
        print(i)
    print()
    
print_matrix(matrix)

det(matrix)


# In[ ]:




# In[ ]:



